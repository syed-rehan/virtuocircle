from api.models import *
from django.forms import ModelForm,Form
from django import forms

class UserForms(ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'password', 'phone','gender', 'dob', 'city','country']

class UserLoginForms(Form):
    email = forms.EmailField(required=True)
    password = forms.CharField()


