from django.contrib.auth import login, authenticate
from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpResponseRedirect, request, HttpResponse
from django.urls import reverse_lazy
from django.utils.crypto import get_random_string
from django.views.generic import View, TemplateView, FormView, ListView
from django.shortcuts import render, redirect

from .hooks import hook_set
from .forms import *
from django.views.generic import CreateView

class SignUpView(CreateView):
    model = User
    form_class = UserForms
    #success_url = reverse_lazy('signup')
    template_name = 'virtuocircle_app/sign_up.html'

    def form_valid(self, form):
        user = self.request.user
        form.instance.user = user
        form.instance.secret_hash = get_random_string(length=32)
        form.save()
        emailverify_context = {'email': form.cleaned_data.get('email'), 'secret_hash': form.instance.secret_hash,
                               'domain': get_current_site(request)}
        hook_set.email_verification(emailverify_context)
        return render(self.request, 'virtuocircle_app/thankyou.html', {'form': form})

class SignInView(FormView):
    model = User
    form_class = UserLoginForms
    success_url = reverse_lazy('signup')
    template_name = 'virtuocircle_app/sign_in.html'


    def form_valid(self, form):
        email = form.cleaned_data.get('email')
        password = form.cleaned_data.get('password')
        user = authenticate(email=email, password=password)
        if user is not None:
            if user.is_verified:
                if user.is_active:
                    login(self.request, user)
                    return HttpResponseRedirect(self.get_success_url())
                else:
                    form.add_error("email", 'Admin Approval Waiting')
                    return render(self.request, 'virtuocircle_app/sign_in.html', {'form': form})
            else:
                form.add_error("email", 'Please Verify Your Email!')
                return render(self.request, 'virtuocircle_app/sign_in.html', {'form': form})


# class Verify(TemplateView):
#     template_name = 'emailverify.html'

class Verify(ListView):

    def get(self, request,secrethash,email, *args, **kwargs):
        user = User.objects.filter(email=email, secret_hash=secrethash, is_deleted=False)
        if user:
            user = User.objects.get(email=email, secret_hash=secrethash)
            if user.is_verified:
                return render(self.request, 'emailverify.html', {'message': "Your Email Already Verified"})
            else:
                user.is_verified = True
                user.save()
                return render(self.request, 'emailverify.html', {'message': "Your Email Verify Successfully"})
        else:
            return render(self.request, 'emailverify.html', {'message': "Bad Request Error 404"})

# def SignUpView(request):
#     if request.method == 'POST':
#         form = UserForms(request.POST)
#         if form.is_valid():
#             form.save()
#     else:
#         form = UserForms()
#     return render(request, 'virtuocircle_app/sign_up.html', {'form': form})