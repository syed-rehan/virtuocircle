from django.shortcuts import render
from rest_framework import viewsets, generics, status
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated,AllowAny
from django.contrib.auth.hashers import make_password,check_password
from django.contrib.auth import authenticate, login, logout
from rest_framework.response import Response
import requests
from virtuocircle import settings
from .serializers import *
from .models import *
from .hooks import hook_set
from django.utils.crypto import get_random_string

# // Generate * Secret_Hash // #


# Create your views here.
class SignUpView(generics.CreateAPIView):
    serializer_class = UserSerializer
    permission_classes = [AllowAny]

    def post(self, request, *args, **kwargs):
        try:
            if User.objects.filter(email=request.data['email']).exists():
                return Response({'message': 'Email is already Exist'}, status=status.HTTP_409_CONFLICT)

            if 'password' in request.data:
                request.data['password'] = make_password(request.data['password'])
                request.data['secret_hash'] = get_random_string(length=32)

            response = self.create(request, *args, **kwargs)
            ctx = response.data
            del response.data["password"]
            response.data['gender'] = User.GENDER_CHOICES[int(response.data['gender']) - 1][1]
            response.data['registration_date'] = response.data["created_at"]

            email_context = {'email': request.data['email'], 'first_name': response.data['first_name'],
                             'last_name': response.data['last_name']}
            hook_set.registration_email(email_context)
            emailverify_context = {'email': response.data['email'], 'secret_hash': response.data['secret_hash'] , 'domain':request.get_host()}
            hook_set.referral_invitation_email(emailverify_context)
            return Response(ctx, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'message': str(e)},status=status.HTTP_400_BAD_REQUEST)


class EmailVerifyView(generics.RetrieveUpdateAPIView):
    serializer_class = EmailVerifySerializer
    permission_classes = [AllowAny]

    def get(self, request,secrethash, email, *args, **kwargs):
        try:
            query_set = User.objects.filter(email=email,secret_hash=secrethash, is_deleted=False)
            if query_set:
                user = User.objects.get(email=email,secret_hash=secrethash)
                user.is_verified = True
                user.save()
                ctx = {secrethash, email}
                return Response(ctx, status=status.HTTP_200_OK)
            else:
                return Response({'message': "Your Email Already Verified"}, status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            return Response({'message': str(e)}, status=status.HTTP_400_BAD_REQUEST)


class SignInView(generics.CreateAPIView):

    def post(self, request, *args, **kwargs):
        try:
            serializer = LoginSerializer(data=request.data)
            if serializer.is_valid(raise_exception=True):
                user = authenticate(request,username=request.data['email'], password=request.data['password'])
                if user is not None:
                    if user.is_verified:
                        if user.is_active:
                            login(request, user)
                            ctx = []
                            ctx = {'id': user.pk,
                                   'first_name': user.first_name,
                                   'last_name': user.last_name,
                                   'email': user.email,
                                   'phone': user.phone,
                                   'is_active': user.is_active,
                                   'image': user.image,
                                   'dob': user.dob,
                                   'gender': User.GENDER_CHOICES[int(user.gender)-1][1],
                                   'city': user.city,
                                   'country': user.country,
                                   }

                            return Response(ctx,status=status.HTTP_200_OK)
                        else:
                            return Response({"message": "User is not activated"},status=status.HTTP_401_UNAUTHORIZED)
                    else:
                        return Response({"message": "User is not verified"}, status=status.HTTP_401_UNAUTHORIZED)
                else:
                    return Response({"message": "Invalid Email or Password"},status=status.HTTP_401_UNAUTHORIZED)
        except Exception as e:
            return Response({'message': str(e)},status=status.HTTP_400_BAD_REQUEST)




# // T o k e n i z a t i on // #


@permission_classes([AllowAny])
class Token(generics.CreateAPIView):

    ''' Gets tokens with username and password. Input should be in the format:{"username": "username", "password": "1234abcd"}'''

    def post(self, request, *args, **kwargs):
        r = requests.post(
            settings.base_url_auth + '/o/token/',
            data={
                'grant_type': 'password',
                'username': request.data['email'],
                'password': request.data['password'],
                'client_id': settings.CLIENT_ID,
                'client_secret': settings.CLIENT_SECRET,
            },
        )
        return Response(r.json())


@permission_classes([AllowAny])
class RefreshToken(generics.CreateAPIView):
    '''
    Registers user to the server. Input should be in the format:
    {"refresh_token": "<token>"}
    '''

    def post(self, request, *args, **kwargs):
        r = requests.post(
            settings.base_url_auth + '/o/token/',
            data={
                'grant_type': 'refresh_token',
                'refresh_token': request.data['refresh_token'],
                'client_id': settings.CLIENT_ID,
                'client_secret': settings.CLIENT_SECRET,
            },
        )
        return Response(r.json())


@permission_classes([AllowAny])
class RevokeToken(generics.CreateAPIView):
    '''
    Method to revoke tokens.
    {"token": "<token>"}
    '''

    def post(self, request, *args, **kwargs):
        r = requests.post(
            settings.base_url_auth + '/o/revoke_token/',
            data={
                'token': request.data['token'],
                'client_id': settings.CLIENT_ID,
                'client_secret': settings.CLIENT_SECRET,
            },
        )
        # If it goes well return sucess message (would be empty otherwise)
        if r.status_code == requests.codes.ok:
            return Response({'message': 'token revoked'}, r.status_code)
        # Return the error if it goes badly
        return Response(r.json(), r.status_code)
