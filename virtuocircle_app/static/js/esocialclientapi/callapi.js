$(document).ready(function(){
//Global Variable Start
     var access_token;
//Global Variable End

//Token Api Start
     function getToken() {
            res = $.ajax({
                        url: "http://localhost:8050/api/v1/token",
                        type: "POST",
                        data: {
                        "email":"rehanhussain1991@gmail.com",
                        "password":"Rehan@125"
                        },
                });
            return res
    }
//Token Api End

//Refresh Token Api Start
    function refreshToken() {

            return $.ajax({
                url: "http://localhost:8050/api/v1/token/refresh",
                type: "POST",
                data: {
                "refresh_token":"PVmoogEf7yKT07GDtzVQLH57pswDcW"
              },
              });
    }
//Refresh Token Api End

//Profile Get Api Start
    function getProfile(){
        getToken().then(function(res) {
             $.ajax({
                  type: "GET",
                  url: "http://localhost:8050/api/v1/users/4/profile",
                  headers: {"Authorization": res.token_type + " " + res.access_token},
                  success: function(data) {
                      console.log(data);
                      document.getElementById("preview").src = data.image_url;
                      $('#firstName').val(data.first_name);
                      $('#lastName').val(data.last_name);
                      $('#postalCode').val(data.postal_code);
                      $('#Gender').val(data.gender);
                      if(data.image_url == ""){
                        $(".profilepht").css("display", "none");
                      }else{
                        $(".profilepht").css("display", "block");
                      }
                  },
                  error: function(data) {
                    console.log(data);
                  }
             });
        });
    }
//Profile Get Api End

//Profile Get Api Call Start
    if (window.location.href.includes("profile")){
        getProfile();
    }
//Profile Get Api Call End

//Profile Post Api Start
    $("#submitProfile").click(function(){
        getToken().then(function(res) {
            var gender;
            if ($('#Gender').val() == "Male" || $('#Gender').val() == "male"){
                gender="1";
            }
            else if ($('#Gender').val() == "Female" || $('#Gender').val() == "female"){
                gender="2";
            }
            else if($('#Gender').val() == "Other" || $('#Gender').val() == "other"){
                gender="3";
            }
            data = {
                "first_name": $('#firstName').val(),
                "last_name": $('#lastName').val(),
                "postal_code": $('#postalCode').val(),
                "gender":gender,
                "image_url": document.getElementById("preview").src,
             }
            $.ajax({
                  type: "PATCH",
                  url: "http://localhost:8050/api/v1/users/4/profile",
                  headers: {"Authorization": res.token_type + " " + res.access_token},
                  data: data,
                  success: function(data) {
                      $(".success").css("display", "block");
                      $(".success").css("opacity", "1");
                      $(".success").css("transition", "opacity 1s");
                      setTimeout(function(){ $(".success").css("opacity", "0"); $(".success").css("transition", "opacity 1s"); }, 3000);
                      console.log(data);
                  },
                  error: function(data) {
                      console.log(data);
                  }
            });
        });

    });
//Profile Post Api End

//Utility Api/Profile Image Start
    $("#profilePhoto").click(function(){
        $('input[type="file"]').change(function(e) {
              var fileName = e.target.files[0].name;
                 var file_data = $('#profilePhoto').prop('files')[0];
                 var form_data = new FormData();
                 form_data.append("base_path","/messangel/profile/13");
                 form_data.append("artifect_type","photo");
                 form_data.append("filename",fileName);
                 form_data.append( "dataFiles",file_data);

              $.ajax({
                  type: "POST",
                  url: "http://51.83.41.210:4000/uploadfile",
                  data: form_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                  contentType: false, // The content type used when sending data to the server.
                  cache: false, // To unable request pages to be cached
                  processData: false, // To send DOMDocument or non processed data file it is set to false
                  success: function(data) {
                      if ('files' in data) {
                        $(".profilepht").css("display", "block");
                        document.getElementById("preview").src = data["files"][0].path;
                      }
                      else {
                        alert('error');
                      }
                  },
                  error: function(data) {
                  console.log(data);
                  }
              });
        });
    });
//Utility Api/Profile Image End

//Change Password Put Api Start
    //Show Password Start
        $(".currentPassword"). click(function () {
            var x = document.getElementById("currentPassword");
            if (x.type === "password") {
                x.type = "text";
            }else {
                x.type = "password";
            }
        });

        $(".newPassword"). click(function () {
            var x = document.getElementById("newPassword");
            if (x.type === "password") {
                x.type = "text";
            }else {
                x.type = "password";
            }
        });

        $(".newConfirmPassword"). click(function () {
            var x = document.getElementById("newConfirmPassword");
            if (x.type === "password") {
                x.type = "text";
            }else {
                x.type = "password";
            }
        });
    //Show Password End

    $("#changePassword").click(function(){
        let currentPassword = $('#currentPassword').val();
        let newPassword = $('#newPassword').val();
        let newConfirmPassword = $('#newConfirmPassword').val();
        if(newPassword == newConfirmPassword){
            alert(2);
            getToken().then(function(res) {
                data = {
                        "password":currentPassword,
                        "new_password":newPassword,
                }
                $.ajax({
                      type: "PUT",
                      url: "http://localhost:8050/api/v1/users/4/change-password",
                      headers: {"Authorization": res.token_type + " " + res.access_token},
                      data: data,
                      success: function(data) {
                          $(".success").css("display", "block");
                          $(".success").css("opacity", "1");
                          $(".success").css("transition", "opacity 1s");
                          setTimeout(function(){ $(".success").css("opacity", "0"); $(".success").css("transition", "opacity 1s"); }, 3000);
                          console.log(data);
                      },
                      error: function(data) {
                          console.log(data);
                      }
                });
            });
        }else{
              $(".failure").css("display", "block");
              $(".failure").css("opacity", "1");
              $(".failure").css("transition", "opacity 1s");
              setTimeout(function(){ $(".failure").css("opacity", "0"); $(".failure").css("transition", "opacity 1s"); }, 3000);
              console.log(data);
        }


    });
//Change Password Put Api End

//Profile Post Api Start
    $("#clarifyAge").click(function(){
        if($('#clarifyAge').prop("checked") == true){
            $('#sendInvite').removeAttr("disabled");
        }else{
            $('#sendInvite').attr("disabled","disabled");
        }
    });
    $("#sendInvite").click(function(){
        getToken().then(function(res) {
            var data = JSON.stringify({
                    "email": $('#inviterEmail').val(),
                    "first_name": $('#inviterFName').val(),
                    "last_name": $('#inviterLName').val(),
                    "user":"4"
                })

//                console.log(data);
            $.ajax({
                  type: "POST",
                  url: "http://localhost:8050/api/v1/users/invite",
                  headers: {"Authorization": res.token_type + " " + res.access_token},
                  dataType: 'json',
                  contentType: 'application/json',
                  data: data,
                  success: function(data) {
                      $(".inviteSuccess").css("display", "block");
                      $(".inviteSuccess").css("opacity", "1");
                      $(".inviteSuccess").css("transition", "opacity 1s");
                      setTimeout(function(){ $(".inviteSuccess").css("opacity", "0"); $(".inviteSuccess").css("transition", "opacity 1s"); }, 3000);
                      console.log(data);
                  },
                  error: function(data) {
                      $(".invitefailure").css("display", "block");
                      $(".invitefailure").css("opacity", "1");
                      $(".invitefailure").css("transition", "opacity 1s");
                      setTimeout(function(){ $(".invitefailure").css("opacity", "0"); $(".invitefailure").css("transition", "opacity 1s"); }, 3000);
                      console.log(data);
                  }
            });
        });

    });
//Profile Post Api End
});

