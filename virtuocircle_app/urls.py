from django.contrib import admin
from django.urls import path, include
from virtuocircle_app import views
from .views import *

urlpatterns = [
    path('sign_up', SignUpView.as_view(), name="signup"),
    path('sign_in', SignInView.as_view(), name="signin"),
    path('emailverify/<secrethash>/<email>', Verify.as_view(), name='verify'),
]
