from django.core.mail import EmailMultiAlternatives, get_connection
from django.template.loader import render_to_string
from account.conf import settings


class AccountDefaultHookSet(object):

    def registration_email(self, ctx):
        subject = render_to_string("subject/registration_success.txt", ctx)
        subject = "".join(subject.splitlines())
        message = render_to_string("email/registration_success.html", ctx)
        msg = EmailMultiAlternatives(subject, 'message', settings.DEFAULT_FROM_EMAIL, [ctx['email']])
        msg.attach_alternative(message, "text/html")
        msg.send()

    def email_verification(self, ctx):
        subject = render_to_string("subject/referral_invite.txt", ctx)
        subject = "".join(subject.splitlines())
        message = render_to_string("email/referral_invite.html", ctx)
        msg = EmailMultiAlternatives(subject, 'message', settings.DEFAULT_FROM_EMAIL, [ctx['email']])
        msg.attach_alternative(message, "text/html")
        msg.send()

    def forgot_password_email(self, ctx):
        subject = render_to_string("subject/forgot_password.txt", ctx)
        subject = "".join(subject.splitlines())
        message = render_to_string("email/forgot_password.html", ctx)
        msg = EmailMultiAlternatives(subject, 'message', settings.DEFAULT_FROM_EMAIL, [ctx['email']])
        msg.attach_alternative(message, "text/html")
        msg.send()


class HookProxy(object):

    def __getattr__(self, attr):
        return getattr(settings.ACCOUNT_HOOKSET, attr)


hook_set = HookProxy()

