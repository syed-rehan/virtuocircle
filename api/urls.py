from django.contrib import admin
from django.urls import path, include

from api.views import *

urlpatterns = [
    path('sign-up', SignUpView.as_view(), name='user_sign_up'),
    path('emailverify/<secrethash>/<email>', EmailVerifyView.as_view(), name='user_verify'),
    path('sign-in', SignInView.as_view(), name='user_sign_in'),

# *****************************Aouth2.0 Authentications*************************

    path('token', Token.as_view(), name='token'),
    path('token/refresh', RefreshToken.as_view(), name='token_refresh'),
    path('token/revoke', RevokeToken.as_view(), name='token_revoke'),
]
