from django.apps import AppConfig


class VirtuocircleAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'virtuocircle_app'
