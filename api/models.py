from django.db import models
from django.contrib.auth.base_user import BaseUserManager
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser

# Create your models here.
class CustomUserManager(BaseUserManager):

    use_in_migrations = True
    """
    Custom user model manager where email is the unique identifiers
    for authentication instead of usernames.
    """
    def create_user(self, email, password, **extra_fields):
        """
        Create and save a User with the given email and password.
        """
        if not email:
            raise ValueError(_('The Email must be set'))
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        """
        Create and save a SuperUser with the given email and password.
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError(_('Superuser must have is_staff=True.'))
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(_('Superuser must have is_superuser=True.'))
        return self.create_user(email, password, **extra_fields)


class BaseModel(models.Model):
    is_deleted = models.BooleanField(null=False, default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

    def delete(self):
        self.is_deleted = True
        self.save()

    def restore(self):
        self.is_deleted = False
        self.save()

class WorkExperience(BaseModel, models.Model):
    title = models.CharField(max_length=25, null=True, blank=True)
    company = models.CharField(max_length=25, null=True, blank=True)
    city = models.CharField(max_length=25, null=True, blank=True)
    country = models.CharField(max_length=25, null=True, blank=True)
    started_at = models.DateField()
    ended_at = models.DateField()
    currently_work = models.BooleanField(default=False)


class Education(BaseModel, models.Model):
    degree = models.CharField(max_length=50, null=True, blank=True)
    field_of_study = models.CharField(max_length=25, null=True, blank=True)
    school = models.CharField(max_length=25, null=True, blank=True)
    school_city = models.CharField(max_length=25, null=True, blank=True)
    school_country = models.CharField(max_length=25, null=True, blank=True)
    started_at = models.DateField()
    ended_at = models.DateField()


class Certification(BaseModel, models.Model):
    title = models.CharField(max_length=50, null=True, blank=True)
    time_period = models.BooleanField(default=False)
    started_at = models.DateField()
    ended_at = models.DateField()
    description = models.CharField(max_length=500, null=True, blank=True)


class Skill(BaseModel, models.Model):
    skill = models.CharField(max_length=50, null=True, blank=True)
    experience = models.IntegerField()


class User(AbstractUser, BaseModel):
    GENDER_CHOICES = (
        ('1', 'Male'),
        ('2', 'Female'),
        ('3', 'Other'),
    )
    username = None
    email = models.EmailField(_('email address'), unique=True)
    phone = models.CharField(max_length=14, null=True, blank=True)
    gender = models.CharField(max_length=10, choices=GENDER_CHOICES, default='Male')
    image = models.CharField(max_length=200,null=True,blank=True)
    dob = models.DateField(null=True,blank=True)
    city = models.CharField(max_length=25, null=True, blank=True)
    country = models.CharField(max_length=25, null=True, blank=True)
    secret_hash = models.CharField(max_length=100, null=True, blank=True)
    is_verified = models.BooleanField(default=False)
    work_experience = models.ManyToManyField(WorkExperience, null=True, blank=True, related_name="work_experience")
    education = models.ManyToManyField(Education, null=True, blank=True, related_name="education")
    certifications = models.ManyToManyField(Certification, null=True, blank=True, related_name="certifications")
    skills = models.ManyToManyField(Skill, null=True, blank=True, related_name="skills")

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    class Meta:
        db_table = 'user'
        verbose_name = 'user'
        verbose_name_plural = 'users'

    def __str__(self):
        return self.email



