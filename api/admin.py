from django.contrib import admin

# Register your models here.
from api.models import *

admin.site.register(User)
admin.site.register(WorkExperience)
admin.site.register(Education)
admin.site.register(Certification)
admin.site.register(Skill)