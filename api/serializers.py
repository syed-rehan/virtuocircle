from rest_framework import serializers
from .models import *

def required(value):
    if value is None:
        raise serializers.ValidationError('This field is required')

class UserSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(validators=[required])
    last_name = serializers.CharField(validators=[required])
    email = serializers.EmailField(validators=[required])
    password = serializers.CharField(validators=[required])
    phone = serializers.CharField(validators=[required])
    gender = serializers.CharField(validators=[required])
    dob = serializers.CharField(validators=[required])
    is_active = serializers.BooleanField(default=False)
    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'email', 'password', 'phone','gender', 'image', 'dob', 'city','country', 'secret_hash', 'is_verified', 'work_experience', 'education', 'certifications', 'skills', 'is_active', 'created_at']

class EmailVerifySerializer(serializers.ModelSerializer):
    email = serializers.EmailField(validators=[required])

    class Meta:
        model = User
        fields = ['id', 'email', 'secret_hash', 'is_verified']

class LoginSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()
    password = serializers.CharField()

    class Meta:
        model = User
        fields = ['email', 'password']
